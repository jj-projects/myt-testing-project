
//
// JS-function for replacement
//

function replaceString(original, pattern, replacement) {
    // Use the replace() method to replace occurrences of 'pattern' with 'replacement'
    return original.replace(pattern, replacement);
}

//
// Example usage:
//

const original = "aapeli";
console.log(`Original: ${original}. After replacement: ` + replaceString(original, /aa/g, "barCode"));
console.log(`Original: ${original}. After replacement: ` + replaceString(original, /ii/g, "barCode"));
console.log(`Original: ${original}. After replacement: ` + replaceString(original, /peli/g, "game"));
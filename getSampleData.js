/**
* This js script is example how we can call specified url and get json data form url
* If process gets json-data via 200 ok response, process will generate simple html-file from it
*
* node getSampleData.js url/web-address 1
*
* @version 1.0.1
*/

// Pick up needed packages
const https = require('https');
const fs = require('fs');

// Pick up arguments from command prompt
const args = process.argv;

/** 
*	Replace the URL with your desired JSON endpoint
*	@constant jsonUrl
*   @type {string}
*   @default
*
*	If you want to get debug printlns to console, you can set printDebug argument value to 1
*	@constant printDebug
*   @type {int}
*   @default
*/

const jsonUrl = args[2];
const printDebug = args[3];

printToConsole(args);

// Do call for specified url
https.get(jsonUrl, (response) => {
    if (response.statusCode === 200) {
        let rawData = '';
		
		// Pick up data to variable, when url call send it caller
        response.on('data', (chunk) => {
            rawData += chunk;
        });
        response.on('end', () => {
            try {
				// Read raw data to file
				printToConsole('Step 1: Check rawdata');
				fs.writeFileSync('rawdata.txt', rawData);
				printToConsole('Step 1: END');
                
				// Convert to array
				printToConsole('Step 2: parse data to array');
				const jsonDataArray = Object.values(JSON.parse(rawData));
				const jsonData = JSON.parse(rawData);
				printToConsole('Step 2: END');
				
				/*
				* Check that parsed data is array, otherwise we have to throw error information
                */
				if (Array.isArray(jsonDataArray)) {
					
					/*
					* Next process will handle json data to map. Then it easier to handle with different table layout snippets
					*/
					
					printToConsole('Step 3: Handle json-data to js map');
					const fullResultMap = new Map();		
					for (const [category, cdata] of Object.entries(jsonData.data.array)) {
						handleEntry(fullResultMap, category, cdata);
					}
					printToConsole('Step 3: END');
					
					/*
					* Next process will create html string about map
					*/
					
					printToConsole('Step 4: Handle array to html content');
					let htmlText = handleMapDataToHtml(fullResultMap);
					printToConsole('Step 4: END');
					
					printToConsole('Step 5: Dump of map');
					printToConsole(fullResultMap);
					printToConsole('Step 5: END');

					/*
					* Write the html-file to file
					* @TODO
					*/
					
					printToConsole('Step 6: Save html-data to file.');
                    fs.writeFileSync('jsonData.html', htmlText);
                    printToConsole('Step 6. END - Data has been successfully written to jsonData.html');
					
                } else {
                    console.error('The retrieved data is not an array.');
                }
            } catch (error) {
				// There is something erroneus on json data from source
                console.error('Error parsing JSON:', error.message);
            }
        });
    } else {
		// If url response code is something else than 200 OK, process shows error on console. 
		// We can also raise up error to cloud for example cloudwatch (aws). We must have attached cloudwatch infra to this one.
		// It can be added to docker, what we can to use for example run this one.
        console.error('Error fetching data. Status code:', response.statusCode);
    }
}).on('error', (error) => {
    console.error('Error fetching data:', error.message);
});

/**
*
* handleMapDataToHtml
*
* @param {string} fullResultMap - .
* @return htmlText - .
*/

function handleMapDataToHtml(fullResultMap) {	
	const arr = [];
	let htmlText = "<!DOCTYPE html><html><style>table, th, td { border:1px solid black; }</style><body>\n";
	htmlText += "<h2>Original Json-data on HTML table</h2>\n";
	htmlText += "<table><tr><th>Header</th><th>Value</th></tr>\n";

	printToConsole("+++++++++");
	const myresultArray = iterateMapRecursive(arr,fullResultMap);
	printToConsole(myresultArray); 
	for (let i = 0; i < myresultArray.length; i++) {
		htmlText += myresultArray[i] + "\n";
	} 
	printToConsole("+++++++++");
	
	htmlText += "</table></body></html>";	
	return htmlText;
}

/**
*
* Function handleEntry
* This function will handle json format data to js map format
*
* @param {string} fullResultMap - result map for parsed json data.
* @param {string} category - could be string, but now it is running number.
* @param {string} cdata - splitted string data from original json-file.
* @return 
*/

function handleEntry(fullResultMap, category, cdata) {

	/*
	* Every entry handle must be own map. 
	* Otherwise we will dismiss information and only last entry will be set on result map
	*/
	
	const entryResultMap = new Map();
	
	/*
	* Show dataArray on screen
	* And set data also to at the end of string
	*/	
	
	printToConsole('\nStart element number:' + category);
	printToConsole("-----------");
	
	/*
	* Process must handle sub "json" content information recursively with function jsonToMap
	*/
	
	for (let [key, value] of Object.entries(cdata)) {
		if (typeof value === 'object' && value !== null) {
			printToConsole("2." + key + ":");
			let myMap = jsonToMap(value);
			printToConsole(myMap);
			entryResultMap.set(key, myMap);
		} else {
			printToConsole("1." + key + ":");
			printToConsole(value);
			entryResultMap.set(key, value);
		}
	}
	
	fullResultMap.set(category, entryResultMap);
	
	printToConsole("-----------");
}

/**
*
* Recursive function to iterate through a JavaScript Map
*
* @param {arr} arr - .
* @param {arr} jsMap - .
* @return arr
*/

function iterateMapRecursive(arr, jsMap, depth = 0) {
    for (const [key, value] of jsMap.entries()) {
		if(depth === 0) {
			printToConsole(`Only Depth: ${depth}`);
			arr.push('<tr bgcolor="blue"><td bgcolor="blue">Next car</td><td bgcolor="blue"></td></tr>');
		}
        if (value instanceof Map) {
            // If the value is another Map (nested), recursively call the function
            iterateMapRecursive(arr, value, depth + 1);
        } else {
            // Print the key and value
            printToConsole(`Key: ${key}, Value: ${value} , Depth: ${depth}`);
			arr.push("<tr><td>" + key +"</td><td>" + value + "</td></tr>");
        }
    }
	return arr;
}

/**
*
* Function jsonToMap
* This function will handle json format data to js map format
*
* @param {string} jsonData - json format data.
* @return {Map} result - Data on Map format
*/

function jsonToMap(jsonData) {
    const result = new Map();
    for (let key in jsonData) {	
        if (jsonData.hasOwnProperty(key)) {
            let value = jsonData[key];
			if (typeof value === 'object' && value !== null) {
				// Recurse into nested objects
				result.set(key, jsonToMap(value));
            } else {
                // Add key-value pair to the array
                result.set(key, value);
            }
        }
    }
    return result;
}

/**
*
* Function printToConsole
* This function will print information to console, if you want specific debug printlns
*
* @param {string} printData - string data, what you want to show on console.
* @return
*/

function printToConsole(printData) {
	if (printDebug === '1') {
		console.log(printData);
	}
}
##
## Function what do replacement on string
##

sub replace_string {
    my ($original, $pattern, $replacement) = @_;
    my $returnValue = $original;
    $returnValue =~ s/$pattern/$replacement/g;
    print "Pattern what process will change: $pattern\n";
    print "Replacement value: $replacement\n";
    print "Original: $original\n";
    print "Modified: $returnValue\n";
    print "-------\n";
    return $returnValue;
}

##
## Example usage:
##

my $newstring = replace_string("aapeli", 'a', 'i');
$newstring = replace_string("aapeli", 'aa', '2');
$newstring = replace_string("aapeli", 'aaaa', 'z');
$newstring = replace_string("aapeli", 'i', '222');
$newstring = replace_string("aapeli", 'al', '333');